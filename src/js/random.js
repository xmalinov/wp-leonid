export const randomNumbers = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const randomString = () => {
  function randStr() {
    return Math.random()
      .toString(36)
      .substring(2, 15);
  }

  return randStr() + randStr();
};
