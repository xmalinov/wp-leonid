import component from "./component";
import '../scss/style.scss';
import { randomNumbers, randomString } from "./random.js";

import passwordIcon from '../images/lock-solid.svg';
    import copyIcon from '../images/copy-solid.svg';

    document.getElementById('copy_icon').src = copyIcon;
    document.getElementById('password_icon').src = passwordIcon;

// import * as generate from './random.js';

//     // Displays a random number between 100 and 10000
//     console.log(generate.randomNumbers(100, 10000));

//     // Displays a random string
//     console.log(generate.randomString());

document.body.appendChild(component());

document.addEventListener("DOMContentLoaded", function() {
  const s = `Random String: <span>${randomString()}</span>`;

  window.setTimeout(function() {
    document.getElementsByTagName("h1")[0].innerHTML = s;
  }, 0);
});
console.log("Hello Project.");
// Displays a random number between 100 and 10000
console.log(randomNumbers(100, 10000));

// Displays a random string
console.log(randomString());
